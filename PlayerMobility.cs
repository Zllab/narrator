﻿using UnityEngine;
using System.Collections;

public class PlayerMobility : MonoBehaviour {

	public float Speed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void FixedUpdate()
	{
		Vector3 inputMousePosition = Input.mousePosition;
		inputMousePosition.z = 9.6f;
		Vector3 mousePosition = Camera.main.ScreenToWorldPoint(inputMousePosition);

		//	//Debug.Log("Mouse World Postion" + mousePosition);
		//	//Debug.Log("Mouse Postion" + Input.mousePosition);

		Quaternion rotation = Quaternion.LookRotation(transform.position - mousePosition, Vector3.forward);
		//	//Debug.Log("Transform Position" + transform.position);
		//	//Debug.Log("Rotation" + rotation);

		transform.rotation = rotation;
		float input = Input.GetAxis("Vertical");
		//Debug.Log("Transform Up " + transform.up);
		Vector2 transformUpNormalized = new Vector2( transform.up.x, transform.up.y);
		transformUpNormalized.Normalize();
		Vector3 force = transformUpNormalized * Speed * input;

		Debug.Log("Force " + force + " input " + input + " tup " + transform.up + " speed " + Speed + " magnitude " + force.magnitude);

		GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
		transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z);
		//Debug.Log("Transform Up " + transform.up);

		//Debug.Log("EulerAngles" + transform.eulerAngles);
		//	Debug.Log("Angular Velocity " + GetComponent<Rigidbody2D>().angularVelocity);
		//		GetComponent<Rigidbody2D>().angularVelocity = 0;
		Debug.Log("");
	}
}
